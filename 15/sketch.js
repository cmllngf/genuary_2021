// Let someone else decide the general rules of your piece.
// From Nokilas : Generate shapes from a picture of me
let canvas;
let img;

let squares = [];

const size = 2;

function preload() {
  img = loadImage("me.jpg");
}

function setup() {
  canvas = createCanvas(800, 800);
  background(10);
  image(img, 0, 0, 800, 800);
  img.loadPixels();
}

function draw() {
  // translate(width / 2, height / 2);
  colorMode(HSB, 255, 255, 255);
  for (let x = 0; x < 800 / size; x++) {
    squares.push([]);
    for (let y = 0; y < 800 / size; y++) {
      const oneD = (y * img.width + x) * 4 * 2.5;
      // const n = noise(x * 0.1, y * 0.1)
      const n = 0;
      let r = img.pixels[oneD * size] + n * 10;
      let g = img.pixels[oneD * size + 1] + n * 50;
      let b = img.pixels[oneD * size + 2] + n * 50;
      // let c = get(x * size + size / 2, y * size + size / 2);
      squares[x].push([r, g, b]);
      fill(r, g, b);
      // stroke(0);
      noStroke();
      rect(x * size, y * size, size, size);
    }
  }
  const xs = [10, 11, 12, 20];
  // for (let k = 0; k < xs.length; k++) {
  //   const x = xs[k];
  for (let x = 0; x < squares.length; x++) {
    for (let j = 0; j < squares[x].length; j++) {
      for (let i = 0; i < squares[x].length - 1; i++) {
        const s1 = squares[x][i];
        const s2 = squares[x][i + 1];
        if (compare2(s1, s2)) {
          squares[x][i] = s2;
          squares[x][i + 1] = s1;
        }
        // if (compare3(s1, s2)) {
        //   squares[x][i] = s2;
        //   squares[x][i + 1] = s1;
        // }
      }
    }
  }

  for (let x = 0; x < squares.length; x++) {
    const element = squares[x];
    for (let y = 0; y < element.length; y++) {
      const s = element[y];
      fill(s[0], s[1], s[2]);
      // stroke(0);
      noStroke();
      rect(x * size, y * size, size, size);
    }
  }
  noLoop();
}

const compare1 = (s1, s2) => s1[0] > s2[0];
const compare2 = (s1, s2) => s1[0] - s2[0] < 10; // 10 pour le sable
const compare3 = (s1, s2) => s1[2] - s2[0] < -10;

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
