// 2D Perspective
let seed,
  t = 0,
  canvas;
const n = 500;
const r = 250;

function setup() {
  canvas = createCanvas(800, 800);
  // capturer.start();
}

function draw() {
  // t = frameCount / 240;
  background(0);
  strokeWeight(1);
  translate(width / 2, height / 2);
  noFill();
  stroke(100);
  circle(0, 0, r * 2);
  for (let i = 0; i < n; i++) {
    stroke(255);
    const a = (TWO_PI / n) * i;
    const x = cos(a) * r;
    const y = sin(a) * r;
    push();
    translate(x, y);
    rotate(a * (180 / PI) + t);
    const nV = map(noise(a, sin(t)), 0, 1, -120, 120);
    line(0, 0, 0, nV);
    pop();
  }

  translate(-width / 2, -height / 2);
  strokeWeight(5);
  stroke(255);
  rect(15, 15, 770, 770);
  // if (t >= TWO_PI) {
  //   console.log(frameCount);
  //   capturer.stop();
  //   capturer.save();
  //   noLoop();
  // }
  // capturer.capture(canvas.canvas);
  t += 0.01;
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
