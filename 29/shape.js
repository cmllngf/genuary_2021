class Shape {
  constructor(x, y, w, h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.r = 0;

    this.init_x = x;
    this.init_y = y;
    this.init_w = w;
    this.init_h = h;
    this.init_r = 0;

    this.aim_x = x;
    this.aim_y = y;
    this.aim_w = w;
    this.aim_h = h;
    this.aim_r = 0;
  }

  aim(options) {
    const { x, y, w, h, r } = { ...this, ...options };
    this.aim_x = x;
    this.aim_y = y;
    this.aim_w = w;
    this.aim_h = h;
    this.aim_r = r;
  }

  display() {
    push();
    noStroke();
    const { x, y, w, h, r } = { ...this };
    // fill(0);
    fill(map(w, 4, 60, 0, 255, true));
    translate(x + w / 2, y + h / 2);
    rotate(r);
    rect(-w / 2, -h / 2, w, h);
    pop();
  }

  transform(t) {
    this.x = map(t, -1, 1, this.init_x, this.aim_x);
    this.y = map(t, -1, 1, this.init_y, this.aim_y);
    this.w = map(t, -1, 1, this.init_w, this.aim_w);
    this.h = map(t, -1, 1, this.init_h, this.aim_h);
    this.r = map(t, -1, 1, this.init_r, this.aim_r);
  }
}
