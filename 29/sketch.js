// Any shape, none can touch.
let canvas;
let shapes = [];
let seed;
let running = false;

function setup() {
  canvas = createCanvas(800, 800);
  for (let i = 0; i < 100; i++) {
    shapes.push(new Shape((i % 10) * 80 + 10, floor(i / 10) * 80 + 10, 60, 60));
  }
  seed = random(99999);
}

function draw() {
  randomSeed(seed);
  // const ttt = roundedSquareWave(frameCount / 90 + .2, 0.1, 1.1, 1 / 3);
  if (frameCount == 180) {
    capturer.start();
    running = true;
  }
  background(255);
  for (let i = 0; i < shapes.length; i++) {
    const s = shapes[i];
    s.display();
    const t = roundedSquareWave(
      frameCount / 90 +
        sin(dist(s.init_x, s.init_y, width / 2, 0) * 0.00015) +
        random(-0.2, 0.2),
      0.1,
      1.1,
      1 / 3
    );
    s.aim({
      x: (i % 10) * 10 + 350,
      y: floor(i / 10) * 10 + 350,
      h: 4,
      w: 4,
      r: (PI / 2) * int(random(3)),
    });
    s.transform(t);
  }
  capturer.capture(canvas.canvas);
  if (frameCount == 180 * 2.5) {
    capturer.stop();
    capturer.save();
    noLoop();
  }
}

const roundedSquareWave = (t, delta, a, f) => {
  return ((2 * a) / Math.PI) * Math.atan(Math.sin(2 * Math.PI * t * f) / delta);
};

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
