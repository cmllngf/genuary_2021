// 2D Perspective
let seed;
let t = 0;
let tpos = 0;
let numberBelts = 1;
let dotsBelts = 800;
let canvas;

function setup() {
  canvas = createCanvas(800, 800);
  colorMode(HSB);
  seed = random(1000);
  numberBelts = 1;
  // numberBelts = int(random(1, 6));
  dotsBelts /= numberBelts;
  capturer.start();
}

function draw() {
  // background(0, 0.5);
  background(0);
  fill(255, 255, 255);
  randomSeed(seed);
  // strokeWeight(1);
  for (let i = 0; i < 500; i++) {
    let [x, y] = [];
    do {
      [x, y] = [random(900), random(900)];
    } while (dist(width + 300, height / 2, x, y) < 610);
    stroke(random(360), random(0, 20), random(60, 100));
    point(x, y);
  }
  noStroke();

  // strokeWeight(2);

  //grid

  // stroke(0, 100, 100);

  // const l1x = 10;
  // const l1y1 = -50;
  // const l1y2 = 950;

  // const l2x = 120;
  // const l2y1 = 300;
  // const l2y2 = 500;

  // line(l1x, l1y1, l1x, l1y2);
  // line(l2x, l2y1, l2x, l2y2);

  // for (let i = 0; i <= 10; i++) {
  //   const x1 = l1x;
  //   const y1 = ((l1y2 - l1y1) / 10) * i;
  //   const x2 = l2x;
  //   const y2 = ((l2y2 - l2y1) / 10) * i;
  //   line(x1, y1 + l1y1, x2, y2 + l2y1);
  // }

  // end grid

  push();
  translate(width + 300, height / 2);
  rotate(-PI / 12);
  noStroke();

  //planet
  for (let i = 0; i < 5550; i++) {
    let a = random(TWO_PI);
    let y = random(-1, 1);
    let r = sqrt(1 - y * y);

    let c = noise(sin(y) * 3, cos(a)) * 360;
    fill(c, 70, 78);

    a -= t / 32;
    let z = sin(a);

    if (z < 0) circle(cos(a) * 600 * r, y * 600 + z * r * 5, 10);
  }

  // belt
  for (let j = 0; j < numberBelts; j++) {
    const beltColor = color(random(360), random(20, 50), random(30, 70));
    fill(beltColor);

    const stepX = 190 / numberBelts;
    const stepY = 20 / numberBelts;

    for (let i = 0; i < dotsBelts; i++) {
      let rx = random(-70 + stepX * j, -60 + stepX * j + stepX);
      let ry = random(-10 + stepY * j, -10 + stepY * j + stepY);
      let a = random(TWO_PI);
      a += t / 16;
      let xpos = cos(a) * (750 + rx);
      let ypos = sin(a) * (50 + ry);
      if (ypos > 0 || dist(0, 0, xpos, ypos) > 605) circle(xpos, ypos, 5);
    }
  }

  // // moons
  for (let j = 0; j < int(random(1, 3)); j++) {
    let startA = random(TWO_PI);
    let yoff = random(-100, -375);
    let size = random(30, 35);
    for (let i = 0; i < 399; i++) {
      fill(color(random(360), 0, random(10, 100)));
      let [a, y] = [random(TWO_PI), random(-1, 1)];
      let r = sqrt(1 - y * y);
      a += t / 4;
      let z = sin(a);
      let zpos = sin(tpos + startA);
      let pSize = map(zpos, -1, 1, size - 20, size + 10);
      const xpos = cos(a) * pSize * r + cos(tpos + startA) * 750 + 0;
      const ypos = y * pSize + z * r * 5 + (0 + yoff) + zpos * 25;
      if (z > 0 && (zpos > 0 || dist(0, 0, xpos, ypos) > 600))
        circle(xpos, ypos, 5);
    }
  }
  pop();

  push();
  translate(100, 100);
  // planet 2
  for (let i = 0; i < 150; i++) {
    let a = random(TWO_PI);
    let y = random(-1, 1);
    let r = sqrt(1 - y * y);

    let c = noise(sin(y) * 3, cos(a)) * 460;
    fill(c, 70, 78);

    a += t / 16;
    let z = sin(a);
    if (z > 0) circle(cos(a) * 20 * r, y * 20 + z * r * 5, 2);
  }
  pop();

  // colorMode(RGB);
  stroke("red");
  strokeWeight(2);
  noFill();
  rect(20, 20, 760, 760);
  rect(20, 740, 350, 40);
  textSize(30);
  fill("red");
  text("SYSTEM : CMLL - 1705", 33, 770);
  // colorMode(HSB);
  t += 0.1;
  tpos += 0.02;
  capturer.capture(canvas.canvas);
  if (t >= TWO_PI * 8) {
    capturer.stop();
    capturer.save();
    noLoop();
  }
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
