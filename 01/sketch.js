// triple nested loop
const steps = 6;
const maxR = 700;
let t = 0;
let seed;

const numFrames = 70;

function setup() {
  createCanvas(800, 800);
  background(0);
  seed = random(99999);
  // saveFrames("out", "png", 1, 54);
}

function draw() {
  randomSeed(seed);
  background(0);
  translate(width / 2, height / 2);
  noFill();
  stroke(255);
  // circle(0, 0, maxR);

  drawCircleOfCircles(0, 0, maxR, maxR * 0.33);
  for (let i = 0; i < steps; i++) {
    const a = (TWO_PI / steps) * i;
    const xc = cos(a) * (maxR / 2 - maxR * 0.165);
    const yc = sin(a) * (maxR / 2 - maxR * 0.165);
    drawCircleOfCircles(xc, yc, maxR * 0.33, maxR * 0.11);
    for (let j = 0; j < steps; j++) {
      const a1 = (TWO_PI / steps) * j;
      const xc1 = cos(a1) * (maxR / 2 - maxR * 0.001);
      const yc1 = sin(a1) * (maxR / 2 - maxR * 0.001);
      drawCircleOfCircles(xc1 + xc, yc1 + yc, maxR * 0.11, (maxR * 0.11) / 3);
      for (let k = 0; k < steps; k++) {
        const a2 = (TWO_PI / steps) * k;
        const xc2 =
          cos(a2) * ((maxR * 0.11) / 3 / 2 - ((maxR * 0.11) / 3) * 0.00001);
        const yc2 =
          sin(a2) * ((maxR * 0.11) / 3 / 2 - ((maxR * 0.11) / 3) * 0.00001);
        drawCircleOfCircles(
          xc2 + xc1,
          yc2 + yc1,
          (maxR * 0.11) / 3,
          (maxR * 0.11) / 3 / 3
        );
      }
    }
  }
  t += 0.02;
  // if (frameCount <= numFrames) {
  //   saveFrames("fr###.gif");
  // }
  // if (frameCount == numFrames) {
  //   println("All frames have been saved");
  //   stop();
  // }
}

function drawCircleOfCircles(x, y, radius, circlesRadius) {
  push();
  translate(x, y);
  for (let i = 0; i < steps; i++) {
    // for (let i = 0; i < random(1, 12); i++) {
    const a = (TWO_PI / steps) * i + t;
    const xc = cos(a) * (radius / 2 - circlesRadius / 2);
    const yc = sin(a) * (radius / 2 - circlesRadius / 2);
    circle(xc, yc, circlesRadius);
  }
  pop();
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
