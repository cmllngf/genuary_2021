// Monochrome gradients without lines.
let canvas;
let visited = [];
let colors = [];
let x = 0;
let y = 0;

const k = 300;
const pixel_size = 1;
const cwidth = 800;
const cheight = 800;
const border = 100;
const countH = 10;
const countV = 10;
let systemsDone = 0;

const poisson_radius = 10;
const poisson_k = 2;

let currentSystemIndex = 0;
let systems = [];

function seed(s, c) {
  visited[s.y * width + s.x] = true;
  colors[s.y * width + s.x] = c;
  stroke(c);
  strokeWeight(pixel_size);
  point(s.x, s.y);
}

function setup() {
  canvas = createCanvas(cwidth, cheight);
  colorMode(HSL);
  background(255);
  const h = random(360);
  const s = 40;
  const b = 58;
  // const s = 70;
  // const b = 58;
  const countX = 1;
  const countY = 2;
  for (let x = 30; x < width - 30; x += 740 / countX) {
    for (let y = 30; y < height - 30; y += 740 / countY + 15) {
      systems.push(
        new System(x, y, 740 / countX, 740 / countY - 15, random(360), s, b)
      );
    }
  }
  // systems.push(new System(30, 30, 740, h, s, b));
  // capturer.start();
}

function draw() {
  for (let i = 0; i < 10; i++) {
    if (!systems[currentSystemIndex].update(visited, colors)) {
      currentSystemIndex++;
      if (!systems[currentSystemIndex]) {
        systemsDone++;
        noLoop();
        // if (systemsDone == 2) {
        //   console.log(frameCount);
        //   // capturer.stop();
        //   // capturer.save();
        //   noLoop();
        //   return;
        // }
      }
    }
  }
  // capturer.capture(canvas.canvas);
}

function keyPressed(key) {
  console.log(key);
  if (key.keyCode === 80) saveCanvas(canvas, "random_walk_color", "png");
}
