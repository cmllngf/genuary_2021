class System {
  constructor(
    x,
    y,
    width,
    height,
    h = random(360),
    s = random(100),
    b = random(100)
  ) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.walkers = [];
    // this.randomHue = random(360);
    // this.randomHue = random(200, 320);
    this.randomHue = h;
    this.h = h;
    this.s = s;
    this.b = b;
    this.seed = random(999999);

    for (let i = 0; i < 1; i++) {
      const wx = int(random(x, x + width));
      const wy = int(random(y, y + height));
      this.walkers.push(this.createWalker(wx, wy, 100000));
    }
  }

  //returns false if finished
  update(visited, colors) {
    randomSeed(this.seed);
    for (let i = 0; i < 100; i++) {
      this.walkers.forEach((walker) => {
        walker.update(
          visited,
          colors,
          pixel_size,
          color(this.randomHue, this.s, this.b)
          // color(random(360), this.s, this.b)
        );
      });
    }
    this.walkers = this.walkers.filter((walker) => walker.alive);
    return this.walkers.length > 0 ? true : false;
  }

  createWalker(x = int(random(width)), y = int(random(height))) {
    const p = createVector(x, y);
    const h = random(360);
    const s = random(100);
    const b = random(100);
    const c = color(this.randomHue, this.s, this.b);
    seed(p, c);
    return new Walker(
      p,
      -1,
      this.x,
      this.x + this.width,
      this.y,
      this.y + this.height,
      color(c)
      // color(random(360), 70, 58)
    );
  }
}
