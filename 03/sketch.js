// make something human
let seed;

function setup() {
  createCanvas(800, 800);
  background(0);
  seed = random(99999);
}

function draw() {
  let noiseV = 0.1;
  randomSeed(seed);
  // background(0);

  noFill();
  stroke(255);
  strokeWeight(2);

  drawByHand(100, 100, 4);
  drawByHand(350, 100, 100);
  drawByHand(600, 100, 5);

  drawByHand(100, 350, 4);
  drawByHand(350, 350, 100);
  drawByHand(600, 350, 5);

  drawByHand(100, 600, 4);
  drawByHand(350, 600, 100);
  drawByHand(600, 600, 5);
  noLoop();
}

function drawByHand(sx, sy, n) {
  push();
  translate(sx + 50, sy + 50);
  rotate(PI / 4 + random(PI));
  beginShape();
  for (let i = 0; i < n; i++) {
    const a = (TWO_PI / n) * i;
    const x = cos(a);
    const y = sin(a);
    // const noiseR = 0;
    const noiseR = map(noise(x, y, n), 0, 1, -15, 15);
    vertex(x * 100 + noiseR, y * 100 + noiseR);
  }
  endShape(CLOSE);
  pop();
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
