// USe Sound
let canvas;
let song;
let a = 0;
let amp;
let fft;
let particles = [];
const maxD = 350;
const maxDD = 500;
const minX = -(maxDD / 2);
const minY = -(maxDD / 2);
const maxX = maxDD / 2;
const maxY = maxDD / 2;
const minX2 = -(maxDD / 1.5);
const minY2 = -(maxDD / 1.5);
const maxX2 = maxDD / 1.5;
const maxY2 = maxDD / 1.5;
const countSpawners = 2;

function setup() {
  canvas = createCanvas(800, 800);
  song = loadSound("fedaden-no-me-siento.mp3", loaded);
  for (let i = 0; i < 500; i++) {
    // const a = random(TWO_PI);
    const a = (TWO_PI / countSpawners) * (i % countSpawners);
    // const distance = int(random(11, maxD));
    const distance = maxD;
    // particles.push({
    //   a,
    //   distance,
    //   prevA: a,
    //   prevDistance: distance,
    //   initA: a,
    //   initDistance: distance,
    //   speed: random(0.2, 1),
    //   speedA: random(0.01, 0.02),
    // });
  }
}

function loaded() {
  song.play();
  // song.jump(65);
  // masterVolume(0.3);
  // masterVolume(0);
  amp = new p5.Amplitude();
  fft = new p5.FFT();
}

function avg(t) {
  let sum = 0;
  for (let item of t) {
    sum += item;
  }
  return sum / t.length;
}

function draw() {
  background(0);
  translate(width / 2, height / 2);
  noStroke();
  fill(255);
  rect(-250, -250, 500, 500);

  fill(0);
  textSize(15);
  // text("CMLL", 210, 245);

  if (song.isPlaying()) {
    let spectrum = fft.analyze();
    // fill(255, 0, 255);
    const average = avg(spectrum.filter((_, i) => i < 10));
    // textSize(20);
    // text(average, -width / 2 + 10, -height / 2 + 20);
    // text(`amp: ${amp.getLevel()}`, -width / 2 + 10, -height / 2 + 40);
    const aSpeed = map(average, 230, 255, 0, 0.03, true);

    if (average > 230 && particles.length < 600) {
      for (let i = 0; i < 2; i++) {
        const distance = maxD;
        const a = (TWO_PI / countSpawners) * int(random(countSpawners));
        particles.push({
          a,
          distance,
          prevA: a,
          prevDistance: distance,
          initA: a,
          initDistance: distance,
          speed: random(0.2, 1),
          s: random(1),
          speedA: random(0.01, 0.02),
        });
      }
    }

    // fill(255);
    noStroke();

    for (let i = 0; i < particles.length; i++) {
      const { a, distance, speed, speedA, s } = particles[i];
      const [x, y] = [cos(a) * distance, sin(a) * distance * 0.35];
      const condi = y > 0 || dist(0, 0, x, y) > 30;
      if (x < maxX2 && x > minX2 && y < maxY2 && y > minY2)
        circle(
          x,
          y + map(distance, 10, 100, 30, 0, true),
          map(y, 150, -150, 9, 3)
        );
      // && x < maxX && x > minX && y < maxY && y > minY
      if (x < maxX && x > minX && y < maxY && y > minY) fill(0);
      else fill(255);
      particles[i] = {
        ...particles[i],
        prevA: a,
        prevDistance: distance,
        a:
          distance < 50
            ? particles[i].initA
            : // : a + map(distance, 9, maxD, 0.05, 0.01),
              a - 0.005 - s * 0.03 - aSpeed,
        distance:
          distance < 50
            ? particles[i].initDistance
            : distance - s * 1.5 - aSpeed * 70,
        // a:
        //   distance < 50
        //     ? particles[i].initA
        //     : // : a + map(distance, 9, maxD, 0.05, 0.01),
        //       a + +speedA + aSpeed,
        // distance:
        //   distance < 50 ? particles[i].initDistance : distance - 0.2 - speed,
      };
    }
    let waveform = fft.waveform();
    noFill();
    beginShape();
    stroke(0);
    for (let i = 0; i < waveform.length; i++) {
      let x = map(i, 0, waveform.length, -100, 100);
      let y = map(waveform[i], -1, 1, 200, 220, true);
      vertex(x, y);
    }
    endShape();
  }
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
// if (song.isPlaying()) {
//   // a += amp.getLevel();
//   // console.log(amp.getLevel());
//   let spectrum = fft.analyze();
//   noStroke();
//   fill(255, 0, 255);
//   const n = 5;

//   // for (let i = 0; i < n; i++) {
//   //   let x = map(i, 0, n, 0, width);
//   //   let h = -height + map(spectrum[i], 0, 255, height, 0);
//   //   rect(x, height, width / n, h);
//   // }
//   const average = avg(spectrum.filter((_, i) => i < 5));
//   textSize(20);
//   text(average, 10, 20);
//   text(`amp: ${amp.getLevel()}`, 10, 40);

//   translate(width / 2, height / 2);
//   // a += map(average, 50, 175, 0, 0.4);
//   const x = cos(a) * 150;
//   const y = sin(a) * 150;
//   fill(255);
//   circle(x, y, 10);
//   // let waveform = fft.waveform();
//   // noFill();
//   // beginShape();
//   // stroke(255);
//   // for (let i = 0; i < waveform.length; i++) {
//   //   let x = map(i, 0, waveform.length, 0, width);
//   //   let y = map(waveform[i], -1, 1, 0, height);
//   //   vertex(x, y);
//   // }
//   // endShape();
// }
