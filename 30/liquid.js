class Liquid {
  constructor(x, y, w, h, c) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.c = c;
  }

  display(c = 0) {
    noStroke();
    fill(c);
    rect(this.x, this.y, this.w, this.h);
  }
}
