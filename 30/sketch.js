// Replicate a natural concept (e.g. gravity, flocking, path following).
let canvas, seed;
let particles = [];
const border = 40;
const attractors = [
  [400, 800],
  // [200, 800],
  // [100, 800],
  // [500, 800],
  // [600, 800],
  // [100, 0],
];
let currentId = 0;
let liquid, liquid2;
let palette = [
  // ,
  // 255,
  // "#D5B5F3",
  "#75D8EC",
  "#F3A9C5",
  // "#A9F3D3",
  // "#F3C4A9",
  // "#E8B466",
  // "#85C784",
];

function setup() {
  canvas = createCanvas(800, 800);
  for (let i = 0; i < 300; i++) {
    particles.push(new Particle({ c: palette[int(random(palette.length))] }));
  }
  background(255);
  seed = random(9999);
  // for (let i = 0; i < 7; i++) {
  //   attractors.push([
  //     int(random(border, width - border)),
  //     int(random(border, height - border)),
  //   ]);
  // }
  liquid = new Liquid(300, 300, 200, 200, 0.05);
  liquid2 = new Liquid(250, 250, 300, 300, 0.05);
}

function draw() {
  randomSeed(seed);
  // background(255);

  liquid2.display(255);
  liquid.display();
  for (let i = 0; i < particles.length; i++) {
    const p = particles[i];
    p.applyForce(createVector(-0.1, 0.1));
    if (particles[i].isInsideLiquid(liquid)) {
      particles[i].dragLiquid(liquid);
      p.display(255);
    } else if (p.isInsideLiquid(liquid2)) {
      p.display(0);
    }
    // if (p.isInsideLiquid({ x: 250, y: 250, w: 300, h: 300 })) {
    //   p.display(255);
    // }
    else {
      p.display();
    }
    p.target(createVector(attractors[currentId][0], attractors[currentId][1]));
    // p.applyForce(createVector(random(-1.1), random(-1.1)));
    p.update();
  }

  noFill();
  stroke(0);
  strokeWeight(7);
  rect(250, 250, 300, 300);
  // strokeWeight(3);
  // rect(border, border, width - border * 2, height - border * 2);

  if (random() > 0.993) {
    console.log("oui");
    currentId++;
    currentId = currentId % attractors.length;
    // currentId = int(random(attractors.length));
  }
}

// function drag() {
//   liquid.display();
//   for (let i = 0; i < particles.length; i++) {
//     if (particles[i].isInsideLiquid(liquid)) {
//       particles[i].dragLiquid(liquid);
//     }

//     // const gravity = createVector(0, 0.5 * particles[i].mass);
//     // particles[i].applyForce(gravity);
//     // particles[i].update();
//     // particles[i].display();
//   }
// }

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
