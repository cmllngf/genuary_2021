class Particle {
  constructor({
    x = random(width),
    y = random(height),
    mass = random(3.5, 6),
    maxSpeed = random(5, 9),
    c = 0,
  }) {
    this.pos = createVector(x, y);
    this.vel = createVector(0, 0);
    this.acc = createVector(0, 0);
    this.mass = mass;
    this.maxSpeed = maxSpeed;
    this.c = c;
    this.a = random(40, 250);
  }

  update() {
    this.vel.add(this.acc);
    this.vel.limit(this.maxSpeed);
    this.pos.add(this.vel);
    this.acc.mult(0);
    this.edges();
  }

  applyForce(force) {
    const f = p5.Vector.div(force, this.mass);
    this.acc.add(f);
  }

  display(c = -1) {
    noStroke();
    const col = color(c == -1 ? this.c : c);
    // col.setAlpha(this.a);
    fill(col);
    // circle(this.pos.x, this.pos.y, map(this.mass, 1, 10, 5, 6));
    if (
      this.pos.x > border &&
      this.pos.x < width - border &&
      this.pos.y > border &&
      this.pos.y < height - border
    )
      circle(this.pos.x, this.pos.y, 10);
  }

  target(target) {
    let dir = p5.Vector.sub(target, this.pos);
    let d = dir.mag();
    dir.setMag(map(d, 0, 50, 0, 10, true));
    let steer = p5.Vector.sub(dir, this.vel);
    steer.limit(0.1);
    this.applyForce(steer);
  }

  edges() {
    if (this.pos.x < border) {
      this.pos.x = width - border;
    }
    if (this.pos.x > width - border) {
      this.pos.x = border;
    }
    if (this.pos.y < border) {
      this.pos.y = height - border;
    }
    if (this.pos.y > height - border) {
      this.pos.y = border + random(5);
    }
  }

  isInsideLiquid(l) {
    if (
      this.pos.x > l.x &&
      this.pos.x < l.x + l.w &&
      this.pos.y > l.y &&
      this.pos.y < l.y + l.h
    )
      return true;
    return false;
  }

  dragLiquid(l) {
    let speed = this.vel.mag();
    let dragMagnitude = l.c * speed * speed;

    let drag = this.vel.copy();
    drag.mult(-1);
    drag.mult(dragMagnitude);
    this.applyForce(drag);
  }
}
