// Draw a line, pick a new color, move a bit.
let canvas;
let l = [],
  t = 0;

function setup() {
  canvas = createCanvas(800, 800);
  background(0);
  l = [];
  for (let i = 0; i < 11; i++) {
    l.push(new Line(150 + i * 50, 100, 150 + i * 50, 700));
  }
  colorMode(HSB);
  capturer.start();
}

function draw() {
  // translate(width / 2, height / 2);
  // background(0);
  for (let i = 0; i < l.length; i++) {
    const element = l[i];
    element.update(t);
    // element.update(sin(t));
    element.display();
  }
  t += 0.05;
  capturer.capture(canvas.canvas);
  if (t >= TWO_PI * 1.5) {
  }
  // noLoop();
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
