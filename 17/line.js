class Line {
  constructor(x1, y1, x2, y2) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
    this.weight = 10;

    this.dots = [];
    const d = dist(x1, y1, x2, y2);
    this.dx = (x2 - x1) / d;
    this.dy = (y2 - y1) / d;
    for (let i = 0; i < 100; i++) {
      const rd = int(random(d));
      const x = this.x1 + this.dx * rd;
      const y = this.y1 + this.dy * rd;
      const nx = map(noise(x, 0), 0, 1, -this.weight / 2, this.weight / 2);
      const ny = map(noise(y, 0), 0, 1, -this.weight / 2, this.weight / 2);
      this.dots.push({
        d: rd,
        x,
        y,
        // xr: random(-this.weight / 2, this.weight / 2),
        // yr: random(-this.weight / 2, this.weight / 2),
        xr: nx,
        yr: ny,
      });
    }
    this.randomHue = int(random(360));
  }

  update(t) {
    for (let i = 0; i < this.dots.length; i++) {
      const dot = this.dots[i];
      const nx = map(
        noise(dot.x, dot.y, t),
        0,
        1,
        -this.weight / 2,
        this.weight / 2
      );
      const ny = map(noise(dot.y, dot.x, t), 0, 1, -100, 100);
      // const n = map(
      //   noise(dot.x, dot.y, t),
      //   0,
      //   1,
      //   -this.weight / 2,
      //   this.weight / 2
      // );
      this.dots[i] = {
        ...dot,
        xr: nx,
        yr: ny,
      };
    }
  }

  updateRandom() {
    for (let i = 0; i < this.dots.length; i++) {
      const dot = this.dots[i];
      this.dots[i] = {
        ...dot,
        xr: random(-this.weight / 2, this.weight / 2),
        yr: random(-this.weight / 2, this.weight / 2),
      };
    }
  }

  display() {
    // stroke(255);
    // line(this.x1, this.y1, this.x2, this.y2);
    this.dots.forEach((dot) => {
      noStroke();
      fill(
        random(this.randomHue, this.randomHue - 100),
        random(20, 68),
        random(40, 80)
      );
      circle(dot.x + dot.xr, dot.y + dot.yr, 2);
    });
  }
}
