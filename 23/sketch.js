// #264653 #2a9d8f #e9c46a #f4a261 #e76f51
let canvas;
let palette = ["#264653", "#2a9d8f", "#e9c46a", "#f4a261", "#e76f51"];

let circles = [];
const particlesCount = 300;

function setup() {
  canvas = createCanvas(800, 800);
  background(0);
  for (let i = 0; i < particlesCount; i++) {
    const a = random(TWO_PI);
    circles.push(
      new Particle(
        createVector(random(30, width - 30), random(30, height - 30)),
        // createVector(
        //   cos(a) * random(150, 170) + width / 2,
        //   sin(a) * random(150, 170) + height / 2
        // ),
        // createVector(
        //   random(width / 2 - 10, width / 2 + 10),
        //   random(height / 2 - 10, height / 2 + 10)
        // ),
        true
      )
    );
  }
}

function draw() {
  // background(0, 30);
  // if(opts.Redraw_Background)
  //   background(10);
  for (let i = 0; i < particlesCount; i++) {
    // if(opts.Draw_Outline)
    // circles[i].display();

    // if(opts.Draw_Center)
    // circles[i].displayCenter();
    circles[i].update();
    for (let j = i + 1; j < particlesCount; j++) {
      if (circles[i].overlaps(circles[j])) {
        circles[i].link(circles[j]);
        circles[i].avoid(circles[j]);
        circles[j].avoid(circles[i]);
      }
    }
  }
  textSize(100);
  for (let i = 0; i < palette.length; i++) {
    const p = palette[i];
    fill(p);
    fill(255);
    noStroke();
    stroke(0);
    strokeWeight(2);
    text(p.toUpperCase(), 200, i * 120 + 200);
  }
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
