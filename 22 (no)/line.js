class Line {
  constructor(x1, y1, x2, y2) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
    this.weight = 50;

    this.dots = [];
    const d = dist(x1, y1, x2, y2);
    this.dx = (x2 - x1) / d;
    this.dy = (y2 - y1) / d;
    for (let i = 0; i < 100; i++) {
      const rd = int(random(d));
      const x = this.x1 + this.dx * rd;
      const y = this.y1 + this.dy * rd;
      const nx = map(noise(x, y, 0), 0, 1, -this.weight / 2, this.weight / 2);
      const ny = map(noise(y, x, 0), 0, 1, -this.weight / 2, this.weight / 2);
      this.dots.push({
        d: rd,
        x,
        y,
        // xr: random(-this.weight / 2, this.weight / 2),
        // yr: random(-this.weight / 2, this.weight / 2),
        xr: nx,
        yr: ny,
      });
    }
  }

  update(t) {
    for (let i = 0; i < this.dots.length; i++) {
      const dot = this.dots[i];
      const n = map(
        noise(dot.x, dot.y, t),
        0,
        1,
        -this.weight / 2,
        this.weight / 2
      );
      this.dots[i] = {
        ...dot,
        xr: n,
        yr: n,
      };
    }
  }

  updateRandom() {
    for (let i = 0; i < this.dots.length; i++) {
      const dot = this.dots[i];
      this.dots[i] = {
        ...dot,
        xr: random(-this.weight / 2, this.weight / 2),
        yr: random(-this.weight / 2, this.weight / 2),
      };
    }
  }

  display() {
    // stroke(255);
    // line(this.x1, this.y1, this.x2, this.y2);
    this.dots.forEach((dot) => {
      noStroke();
      fill(255);
      circle(dot.x + dot.xr, dot.y + dot.yr, 5);
    });
  }
}
