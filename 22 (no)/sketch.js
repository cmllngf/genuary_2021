// Draw a line, pick a new color, move a bit.
let canvas;
let l = [],
  t = 0;

function setup() {
  canvas = createCanvas(800, 800);
  background(10);
  l = [new Line(10, 30, 250, 550), new Line(110, 110, 260, 260)];
}

function draw() {
  background(10);
  for (let i = 0; i < l.length; i++) {
    const element = l[i];
    element.update(t);
    element.display();
  }
  t += 0.1;
  // noLoop();
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
