// One process grows, another process prunes
let canvas;
let visited = [];
let colors = [];
let x = 0;
let y = 0;

const k = 300;
const pixel_size = 3;
const cwidth = 800;
const cheight = 800;
const border = 100;
const countH = 10;
const countV = 10;
let systemsDone = 0;

const poisson_radius = 10;
const poisson_k = 2;

let currentSystemIndex = 0;
let systems = [];

function seed(s, c) {
  visited[s.y * width + s.x] = true;
  colors[s.y * width + s.x] = c;
}

function setup() {
  canvas = createCanvas(cwidth, cheight);
  colorMode(HSB);
  background(0);
  for (let x = 0; x < 1; x++) {
    systems.push(new System(10, 10, 780, 780));
  }
  capturer.start();
}

function draw() {
  for (let i = 0; i < 10; i++) {
    if (!systems[currentSystemIndex].update(visited, colors)) {
      currentSystemIndex++;
      if (!systems[currentSystemIndex]) {
        systemsDone++;
        if (systemsDone == 2) {
          console.log(frameCount);
          capturer.stop();
          capturer.save();
          noLoop();
          return;
        } else {
          console.log("again");
          currentSystemIndex = 0;
          visited = [];
          colors = [];
          systems = [new System(10, 10, 780, 780)];
        }
      }
    }
  }
  capturer.capture(canvas.canvas);
}

function keyPressed(key) {
  console.log(key);
  if (key.keyCode === 80) saveCanvas(canvas, "random_walk_color", "png");
}
