class System {
  constructor(x, y, size) {
    this.x = x;
    this.y = y;
    this.size = size;
    this.walkers = [];
    this.randomHue = random(360);

    for (let i = 0; i < 5; i++) {
      const wx = int(random(x, x + size));
      const wy = int(random(y, y + size));
      this.walkers.push(this.createWalker(wx, wy));
    }
  }

  //returns false if finished
  update(visited, colors) {
    for (let i = 0; i < 100; i++) {
      this.walkers.forEach((walker) => {
        walker.update(
          visited,
          colors,
          pixel_size,
          color(this.randomHue, 70, 58)
        );
      });
    }
    this.walkers = this.walkers.filter((walker) => walker.alive);
    return this.walkers.length > 0 ? true : false;
  }

  createWalker(x = int(random(width)), y = int(random(height))) {
    const p = createVector(x, y);
    const h = random(360);
    const s = random(100);
    const b = random(100);
    const c = color(h, s, b);
    seed(p, c);
    return new Walker(
      p,
      -1,
      this.x,
      this.x + this.size,
      this.y,
      this.y + this.size
    );
  }
}
