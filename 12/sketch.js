// Use an API
let canvas;
const apiKey = "WP9fA2qBHdRY66ESEV9mJVyTFxAroMPHdCcBauH6";

let communes;
let points = [];

let minLon;
let minLat;
let maxLon;
let maxLat;

let drawn = false;
let startLine;
let endLine;
let currentP;
let drawingLine = false;

function preload() {
  // let url = `https://geo.api.gouv.fr/communes/59140/?&fields=code,nom,contour`;
  let url = `https://geo.api.gouv.fr/departements?fields=code`;
  httpGet(url, "json", false, (departements) => {
    const departement = departements[int(random(departements.length))];
    // let url2 = `https://geo.api.gouv.fr/departements/${departement.code}/communes?fields=code,nom,contour`;
    let url2 = `https://geo.api.gouv.fr/departements/59/communes?fields=code,nom,contour,centre`;
    httpGet(url2, "json", false, (res) => {
      communes = res;
      communes.forEach((commune) => {
        if (commune.contour.type === "Polygon") {
          commune.contour.coordinates.forEach((coord) =>
            coord.forEach((c) => points.push(c))
          );
        }
      });
      minLon = points[0][0];
      maxLon = points[0][0];
      minLat = points[0][1];
      maxLat = points[0][1];
      points.forEach((point) => {
        if (point[0] < minLon) minLon = point[0];
        else if (point[0] > maxLon) maxLon = point[0];

        if (point[1] < minLat) minLat = point[1];
        else if (point[1] > maxLat) maxLat = point[1];
      });
      endLine = points[int(random(points.length))];
    });
  });
}
// let url2 = `https://geo.api.gouv.fr/departements/${departements[0].code}/communes?fields=code,nom,contour`;
// httpGet(url2, "json", false, (res) => {
//   communes = res;
// });

function setup() {
  canvas = createCanvas(1080, 1080);
  background(0);
  // frameRate(10);
}

function draw() {
  translate(width / 2, height / 2);
  noStroke();
  fill(255, 2);
  if (!!communes) {
    // console.log({ communes });
    // const cP = points.filter((_, i) => i % 10 == 0);
    // for (let i = 0; i < cP.length; i++) {
    //   const p = cP[i];
    //   const x = map(p[0], minLon, maxLon, -350, 350);
    //   const y = map(p[1], minLat, maxLat, 350, -350);
    //   circle(x, y, 3);
    // }
    // noLoop();
    for (let index = 0; index < 1000; index++) {
      if (!drawingLine) {
        drawingLine = true;
        startLine = endLine;
        currentP = [
          map(startLine[0], minLon, maxLon, -500, 500),
          map(startLine[1], minLat, maxLat, 500, -500),
        ];
        endLine = points[int(random(points.length))];
      } else {
        const x = map(startLine[0], minLon, maxLon, -500, 500);
        const y = map(startLine[1], minLat, maxLat, 500, -500);
        const xe = map(endLine[0], minLon, maxLon, -500, 500);
        const ye = map(endLine[1], minLat, maxLat, 500, -500);
        const d = dist(x, y, xe, ye);
        if (dist(xe, ye, currentP[0], currentP[1]) < 10) {
          drawingLine = false;
        } else {
          const dx = (xe - x) / (d / 1);
          const dy = (ye - y) / (d / 1);
          currentP = [currentP[0] + dx, currentP[1] + dy];
          circle(currentP[0], currentP[1], 2);
        }
      }
    }
    if (!drawn) {
      drawn = true;
      for (let i = 0; i < communes.length; i++) {
        const c = communes[i];
        const xc = map(c.centre.coordinates[0], minLon, maxLon, -500, 500);
        const yc = map(c.centre.coordinates[1], minLat, maxLat, 500, -500);
        fill(255, 0, 0, 180);
        circle(xc, yc, 2);
      }
    }
  }
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
