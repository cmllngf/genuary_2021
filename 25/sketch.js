// Make a grid of permutations of something.
let canvas,
  seed,
  t = 0;
let tSize = 10;
let words = [];
const ws = [""];
let palette = ["#264653", "#2a9d8f", "#e9c46a", "#f4a261", "#e76f51"];
let palette2 = ["#D5B5F3", "#75D8EC", "#F3A9C5", "#A9F3D3", "#F3C4A9"];

// function setup() {
//   canvas = createCanvas(800, 800);
//   colorMode(HSB);
//   seed = random(999);
//   for (let x = 0; x < width; x += tSize * 3) {
//     for (let y = tSize; y < height; y += tSize + 1.5) {
//       words.push({
//         x,
//         y,
//         worded: random() > 0.99,
//         word: ws[int(random(ws.length))],
//       });
//     }
//   }
// }

// function draw() {
//   // randomSeed(seed);
//   background(0);
//   textSize(tSize);
//   for (let i = 0; i < words.length; i++) {
//     const word = words[i];
//     // fill(noise(cos(i) * 1.3, sin(i) * 1.3, t) * 560, 80, 78);
//     // fill(noise(i * 0.1, t) * 960, 100, 100);
//     // fill(255);

//     if (
//       dist(word.x, word.y, width / 2, height / 2) <
//       map(noise(word.x, word.y), 0, 1, 100, 500)
//     ) {
//       // fill(255);
//     } else {
//       // fill(noise(cos(i) * 1.3, sin(i) * 1.3, t) * 560, 80, 78);
//       // fill(10);
//     }

//     // fill(noise(word.x * 0.005, word.y * 0.005, t) * 360, 100, 100);
//     text(word.worded ? word.word : "CMLL", word.x, word.y);
//     if (word.worded ? random() > 0.99 : random() > 0.9999) {
//       words[i] = {
//         ...word,
//         worded: !word.worded,
//       };
//     }
//   }
//   t += 0.005;
// }

const col = 25;
const row = 25;
const noiseMult = 0.001;
const margin = 50;
function setup() {
  canvas = createCanvas(800, 800);
  seed = random(999);
}

function draw() {
  randomSeed(seed);
  background(255);
  const w = (width - margin * 2) / col;
  const h = (height - margin * 2) / row;
  // drawHash(0, 0, 0, w, h);
  for (let x = margin; x < width - margin; x += w) {
    for (let y = margin; y < height - margin; y += h) {
      drawHash(
        x,
        y,
        noise(x * noiseMult, y * noiseMult, t) * TWO_PI,
        w,
        h,
        random(3, 8)
      );
    }
  }
  noLoop();
  t += 0.003;
}

function drawHash(x, y, a, w, h, n = 5) {
  push();
  translate(x + w / 2, y + h / 2);
  rotate(a);
  stroke(255);
  strokeWeight(3);
  stroke(palette2[int(random(palette2.length))]);
  // stroke(palette2[int(n % palette2.length)]);
  for (let i = 0; i < n; i++) {
    const xx = (w / n) * i;
    line(xx - w / 2, -h / 2, xx - w / 2, h - h / 2);
  }
  pop();
}

function periodicFunction(p, seed, x, y) {
  const radius = 0.5;
  const scl = 0.0001;
  return noise(
    seed + radius * cos(TWO_PI * p),
    radius * cos(TWO_PI * p),
    scl * x,
    scl * y
  );
}

function offset(x, y) {
  return 0.015 * dist(x, y, width / 2, height / 2);
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
