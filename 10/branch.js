class Branch {
  constructor() {
    this.x = random(-20, 20);
    this.y = 0;
    this.r = random(4, 7);
    this.maxL = random(1.5, 3);
    this.seed = random(999);
    this.t = 0;
    this.hue = random(360);
    this.gs = random(10, 250);
  }

  update(t) {
    if (t < this.maxL) {
      this.y -= 2;
      push();
      const a = map(noise(t * 0.8, this.seed), 0, 1, -PI / 12, PI / 12);
      rotate(a);
      noStroke();
      fill(this.gs);
      circle(this.x, this.y, this.r);
      pop();
      this.t = t;
    } else {
      push();
      colorMode(HSB);
      fill(this.hue, 50, 80);
      noStroke();
      const a = map(noise(this.t * 0.8, this.seed), 0, 1, -PI / 12, PI / 12);
      rotate(a);
      circle(this.x, this.y, this.r * 3);
      pop();
    }
  }
}
