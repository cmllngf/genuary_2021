// Tree
let canvas;
let t = 0;

const branches = [];

function setup() {
  canvas = createCanvas(800, 800);
  background(1);

  for (let i = 0; i < 30; i++) {
    branches.push(new Branch());
  }
  capturer.start();
}

function draw() {
  translate(width / 2, height / 1.1);

  for (let i = 0; i < branches.length; i++) {
    branches[i].update(t);
  }
  t += 0.01;
  capturer.capture(canvas.canvas);
  if (t >= 6) {
    capturer.stop();
    capturer.save();
    noLoop();
  }
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
