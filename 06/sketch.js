// Triangle subdivision
let t = 0;
let canvas;

function setup() {
  canvas = createCanvas(800, 800);
  capturer.start();
}

function draw() {
  background(0);
  frameRate(60);
  translate(width / 2, height / 1.6);

  noStroke();
  fill(255);

  for (let i = 0; i < 1.5; i += 0.15) {
    drawLine(-260 * i, 150 * i, 260 * i, 150 * i, 10, 4);
    drawLine(0 * i, -300 * i, -260 * i, 150 * i, 10, 4);
    drawLine(260 * i, 150 * i, 0 * i, -300 * i, 10, 4);
  }

  capturer.capture(canvas.canvas);
  if (t == 900) {
    console.log(frameCount);
    capturer.stop();
    capturer.save();
    noLoop();
  }
  t += 1;
}

function drawLine(x, y, x2, y2, pDots, pEmpty) {
  const nbDots = (dist(x, y, x2, y2) % t) / 2;
  const dx = (x2 - x) / nbDots;
  const dy = (y2 - y) / nbDots;

  let [xx, yy] = [x, y];
  const dash = pDots + pEmpty;
  const offset = t % dash;
  let printed = offset;

  for (let i = 0; i < nbDots; i++) {
    xx += dx;
    yy += dy;

    if (printed % dash < pDots) circle(xx, yy, 2);
    printed++;
  }
}
