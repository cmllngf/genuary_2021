// The inconsistency principle
let canvas;
let t = 0;
const radius = 250;
const ys = 10;

function setup() {
  canvas = createCanvas(800, 800);
  seed = random(99999);
  // capturer.start();
}

function draw() {
  randomSeed(seed);
  background(0);

  const rectNb = 10;
  const rectW = (width - 40) / rectNb;
  // const c1 = color("#561705");
  // const c2 = color("#E69513");
  // const c1 = color("#060405");
  // const c2 = color("#A0A8D9");
  const c1 = color("#00132B");
  const c2 = color("#30173F");
  for (let i = 0; i < rectNb; i++) {
    noStroke();
    if (i != 6) fill(lerpColor(c1, c2, map(i, 0, rectNb, 0, 1)));
    // else fill("#FED403");
    // else fill("#E7C9C7");
    else fill("#E0B6A0");
    rect(i * rectW + 20, 0, rectW, height);
  }

  push();

  translate(width / 2, height / 2);
  noStroke();
  fill(10, 10, 10);
  // fill(155, 80, 70);
  fill(255);
  beginShape();
  for (let i = 0; i < 1000; i++) {
    let a = random(TWO_PI);
    let y = random(-1, 1);
    let r = sqrt(1 - pow(y, 2));

    a -= t;
    let z = sin(a);

    vertex(cos(a) * radius * r, y * radius + z * r * 5);
  }
  endShape();

  pop();

  fill(10, 10, 10);
  noStroke();
  textSize(15);
  // textAlign(CENTER, CENTER);
  const str = "The inconsistency principle - CMLL";
  // text(str.toUpperCase(), 495, 775);

  noStroke();
  fill(255);
  rect(0, 0, 20, height);
  rect(0, height - 20, width, 20);
  rect(width - 20, 0, 20, height);
  rect(0, 0, width, 20);
  noLoop();

  // capturer.capture(canvas.canvas);
  // if (t >= TWO_PI) {
  //   // console.log("done");
  //   // frameCount = 0;
  //   console.log(frameCount);
  //   capturer.stop();
  //   capturer.save();
  //   noLoop();
  // }

  t += 0.01;
  // noStroke();
  // fill(255);

  // beginShape();
  // for (let i = 0; i < 100; i++) {
  //   let a = random(TWO_PI);
  //   // let y = random(-1, 1);
  //   let y = -1 + (2 / ys) * int(random(1, ys));
  //   let r = sqrt(1 - y * y);

  //   // a -= t / map(a, 0, TWO_PI, 16, 4);
  //   a -= y > 0 ? -t / 8 : t / 8;
  //   let z = sin(a) * 10;

  //   // if (z < 0)
  //   vertex(cos(a) * radius * r, y * radius + z * r * 5);
  // }
  // endShape(CLOSE);
  // stroke(250);
  // strokeWeight(4);
  // noFill();
  // for (let i = 0; i < 500; i++) {
  //   let a = random(TWO_PI);
  //   // let y = random(-1, 1);
  //   let y = -1 + (2 / ys) * int(random(1, ys));
  //   let r = sqrt(1 - y * y);

  //   // a -= t / map(a, 0, TWO_PI, 16, 4);
  //   a -= y > 0 ? -t : t;
  //   // a -= y > 0 ? -t : t;
  //   let z = sin(a) * 10;

  //   const rr = map(z, 10, -10, 1.3, 1.1);
  //   // if (t >= 5)
  //   // line1(
  //   //   cos(a) * radius * r,
  //   //   y * radius + z * r * 5,
  //   //   cos(a) * radius * r * rr,
  //   //   y * radius + z * r * 5 * rr
  //   // );
  //   // // else
  //   // line2(
  //   //   cos(a) * radius * r,
  //   //   y * radius + z * r * 5,
  //   //   cos(a) * radius * r * rr,
  //   //   y * radius + z * r * 5 * rr
  //   // );
  // }
}

const line1 = (x1, y1, x2, y2) => {
  for (let i = 0; i <= 1; i += random(0.1, 0.4)) {
    const x = lerp(x1, x2, i);
    const y = lerp(y1, y2, i);
    point(x, y);
  }
};

// IDEA
const line2 = (x1, y1, x2, y2) => {
  const d = dist(x1, y1, x2, y2);
  // const wtf = d / (d * map(min(t, 4), 0, 1, 1, 0));
  const wtf = d / (d * map(min(t, 6), 0, 6, 6, 0));
  const dx = (x2 - x1) / wtf;
  const dy = (y2 - y1) / wtf;
  for (let i = 0; i < d; i += 100) {
    point(x1 + dx * i, y1 + dy * d);
  }
};

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
