// function f(x) {
//   DRAW(x);
//   f(1 * x / 4);
//   f(2 * x / 4);
//   f(3 * x / 4);
// }
let canvas;
let t = 0;
let gx = 0;
let seed;
let lastGX = gx;

const pinkPalette = ["#eee3e7", "#ead5dc", "#eec9d2", "#f4b6c2", "#f6abb6"];
const pinkPalette2 = ["#ff77aa", "#ff99cc", "#ffbbee", "#ff5588", "#ff3377"];
const redBluePalette = ["#051e3e", "#251e3e", "#451e3e", "#651e3e", "#851e3e"];
const yesP = ["#E09FAF", "#6DE3C7", "#7FB5A8", "#765DBA", "#A6AD93"];

const palette = redBluePalette;

function setup() {
  canvas = createCanvas(800, 800);
  background(0);
  colorMode(HSB);
  seed = random(99999);
  capturer.start();
}

function draw() {
  randomSeed(seed);
  translate(width / 2, height / 2);
  scale(0.5, 0.5);
  const sca = constrain(t * 5000, 0, 1);
  // scale(sca, sca);
  // background(255);
  background(0);
  // background(palette[0]);
  gx /= 100;
  f(400, 7);
  t += 0.000001;
  capturer.capture(canvas.canvas);
  if (frameCount == 240) {
    // console.log("done");
    // frameCount = 0;
    console.log(frameCount);
    capturer.stop();
    capturer.save();
  }
  // noLoop();
}

function f(x, n) {
  DRAW(x);
  if (n <= 0) return;
  f((1 * x) / 4, n - 1);
  f((2 * x) / 4, n - 1);
  f((3 * x) / 4, n - 1);
}

function DRAW(x) {
  // pop();
  noStroke();
  rotate(x / 100 - t);
  const size = constrain(x, 5, 250);
  if (size <= 8) {
    // fill(155);
    // fill(map(x, 0, (3 * 400) / 4, 155, 255, false));
    // fill(map(x, 0, 8, 55, 0, false));
    fill(map(x, 0, 8, 155, 200, false));
  } else {
    // fill(palette[int(random(palette.length))]);
    // fill(palette[floor(map(size, 9, 250, 1, palette.length - 1))]);
    // fill(map(x, 0, (3 * 400) / 4, 150, 55, false));
    fill(x);
  }
  beginShape();
  const nn = random(5, 30);
  for (let i = 0; i < nn; i++) {
    const a = (TWO_PI / nn) * i;
    // const nV = map(noise(sin(a) * 0.1, size), 0, 1, -size, size);
    const nV = 0;
    vertex(cos(a) * (size / 3) + gx + nV, sin(a) * (size / 3) + gx + nV);
  }
  endShape(CLOSE);
  // rect(gx, gx, size, size);
  // circle(gx, gx, constrain(x, 3, 250 - t * 1000000));
  lastGX = gx;
  gx += x;
  stroke(255);
  line(lastGX, x, lastGX, x);
  if (gx > 800) gx = 0;
  else if (gx < 0) gx = 800;
  // push();
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
