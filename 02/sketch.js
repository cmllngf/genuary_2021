// rule 30
let noiseValue = 0;
var ca;

function setup() {
  createCanvas(800, 800);
  background(0);
  ca = new CA();
}

function draw() {
  translate(0, height / 2);
  ca.display();
  if (ca.generation / 2 < height / ca.w) {
    ca.generate();
  }
}

function CA() {
  this.w = 1;
  this.lastCell = null;
  this.cells = new Array(width / this.w);
  for (var i = 0; i < this.cells.length; i++) {
    this.cells[i] = 0;
  }
  this.cells[this.cells.length / 2] = 1;
  this.generation = 0;

  this.ruleset = [0, 0, 0, 1, 1, 1, 1, 0];

  this.generate = function () {
    var nextgen = [];
    for (var i = 0; i < this.cells.length; i++) {
      noiseValue += 0.0001;
      nextgen[i] = 0;
    }
    for (var i = 1; i < this.cells.length - 1; i++) {
      var left = this.cells[i - 1];
      var me = this.cells[i];
      var right = this.cells[i + 1];
      nextgen[i] = this.rules(left, me, right);
    }
    this.cells = nextgen;
    this.generation++;
  };

  // This is the easy part, just draw the cells
  this.display = function () {
    for (var i = 0; i < this.cells.length; i++) {
      if (this.cells[i] == 1) {
        if (this.generation % 2 == 0) stroke(noise(noiseValue * 0.1) * 255);
        else stroke(200);
      } else {
        stroke(0);
      }
      // noStroke();

      if (this.lastCell === null) {
        this.lastCell = [
          i * this.w,
          this.generation * this.w * (this.generation % 2 ? -1 : 1),
        ];
      } else {
        line(
          i * this.w + noise(noiseValue) * 300,
          this.generation * this.w * (this.generation % 2 ? -1 : 1),
          this.lastCell[0],
          this.lastCell[1]
        );
        this.lastCell = null;
      }

      // rect(
      //   i * this.w + (this.generation % 2 == 0 ? noise(noiseValue) * 50 : 0),
      //   this.generation * this.w * (this.generation % 2 ? -1 : 1) +
      //     (this.generation % 2 == 0 ? noise(noiseValue) * 3 : 0),
      //   this.w,
      //   this.w
      // );
    }
  };

  this.rules = function (a, b, c) {
    if (a == 1 && b == 1 && c == 1) return this.ruleset[0];
    if (a == 1 && b == 1 && c === 0) return this.ruleset[1];
    if (a == 1 && b === 0 && c == 1) return this.ruleset[2];
    if (a == 1 && b === 0 && c === 0) return this.ruleset[3];
    if (a === 0 && b == 1 && c == 1) return this.ruleset[4];
    if (a === 0 && b == 1 && c === 0) return this.ruleset[5];
    if (a === 0 && b === 0 && c == 1) return this.ruleset[6];
    if (a === 0 && b === 0 && c === 0) return this.ruleset[7];
    return 0;
  };
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
