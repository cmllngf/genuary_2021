// Interference patterns
const lines = [];
const scl = 40;
const circleR = 7;
const [rows, cols] = [800 / scl, 800 / scl];
let canvas;

let t = 0;

function setup() {
  canvas = createCanvas(800, 800);
  background(10);
  capturer.start();
  frameRate(60);

  // for (let i = 0; i < rows; i++) {
  //   for (let j = 0; j < cols; j++) {
  //     lines.push({
  //       x: j * scl,
  //       y: i * scl,
  //     });
  //   }
  // }
}

function draw() {
  background(0);

  push();
  translate(width / 2, height / 2);
  drawGrid(0, 0);
  pop();

  push();
  translate(width / 2, height / 2);
  rotate(t);
  drawGrid(0, 0);
  pop();

  push();
  translate(width / 2, height / 2);
  rotate(-t);
  drawGrid(0, 0);
  pop();

  // for (let i = 0; i < lines.length; i++) {
  //   const l = lines[i];
  //   const noiseValue = myNoise(i * 0.1, t);
  //   if (noiseValue > 0.05) {
  //     colorMode(HSB);
  //     noStroke();
  //     fill(55);
  //     rect(
  //       l.x + scl / 2 - circleR / 2,
  //       l.y + scl / 2 - circleR / 2,
  //       circleR,
  //       circleR
  //     );
  //     fill(255);
  //     rect(l.x + scl / 2, l.y + scl / 2, circleR, circleR);
  //     // fill(360, 100, 100);
  //     // circle(l.x + scl / 2, l.y + scl / 2, circleR);
  //     // fill(155, 100, 100);
  //     // circle(l.x + scl / 3, l.y + scl / 3, circleR);
  //     // fill(55, 100, 100);
  //     // circle(l.x + scl / 6, l.y + scl / 6, circleR);
  //   } else {
  //     noStroke();
  //     fill(255);
  //     circle(l.x + scl / 2, l.y + scl / 2, circleR);
  //   }
  // }
  // noLoop();
  t += 0.01;
  capturer.capture(canvas.canvas);
  if (t >= PI) {
    console.log(frameCount);
    capturer.stop();
    capturer.save();
    noLoop();
  }
}

function drawGrid(x, y) {
  const w = 10;
  const ws = 10;
  const h = 2000;
  const n = 2000;
  for (let i = 0; i < n; i++) {
    noStroke();
    fill(255);
    rect(x + i * (w + ws) - (n * (w + ws) - w) / 2, y - h / 2, w, h);
  }
}

function myNoise(p, p2 = 0, p3 = 0) {
  const scl = 10;
  return pow((noise(scl * p, scl * p2, p3) + 1) / 2.0, 20.0);
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
