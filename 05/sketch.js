// code golf
let ps = [];
const ds = 300;

function setup() {
  createCanvas(800, 800);
  for (let i = 0; i < 2000; i++) {
    const a = [0, PI][int(random(2))];
    ps.push({ sA: a, cA: a, s: ds, sp: random(1) });
  }
}

function draw() {
  background(0);
  translate(width / 2, height / 2);
  noStroke();
  fill(255);
  circle(0, 0, 10);
  for (let i = 0; i < ps.length; i++) {
    const p = ps[i];
    const [x, y] = [cos(p.cA) * p.s, sin(p.cA) * p.s];
    const d = dist(x, y, 0, 0);
    circle(x, y, map(d, ds, 0, 1, 5));
    ps[i] = {
      ...p,
      ...(d < 5
        ? { cA: p.sA, s: ds }
        : { cA: p.cA - 0.005 - p.sp * 0.03, s: p.s - p.sp }),
    };
  }
}
