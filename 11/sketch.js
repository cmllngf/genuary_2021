// Use something other than a computer as an autonomous process (or use a non-computer random source)
let canvas;
let i = 0;
// let text =
//   "Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world Around the world, around the world";
let text =
  "Eg songane søkte Eg songane sende då den djupaste brunni gav meg dråpar så ramme av Valfaders pant Alt veit eg, Odin kvar du auge løynde Kven skal synge meg i daudsvevna slynge meg når eg helvegen går og dei spora eg trår er kalde, så kalde Årle ell i dagars hell enn veit ravnen om eg fell Når du ved helgrindi står og når du laus deg må rive skal eg fylgje deg yver gjallarbrui med min song Du blir løyst frå banda som bind deg Du er løyst frå banda som batt deg";
// let text =
//   "Je suis venu te dire faut être heureux Alors arrête de courir et de croire que t'iras mieux Je suis venu te dire faut être heureux On n'a qu'à sourire et penser qu'c'est qu'un jeu Dans mon canapé, une bombe devant les yeux Un camouflet, un mensonge pour croire en dieu Écœuré, je vomis mes rêves trop pieux J'suis juste venu te dire faut être heureux Y a des paroles en l'air, un souffle, des gens hargneux Du vent, des idéaux, saisis au vol pour aller mieux J'ai cru voir la mer Oh mon dieu, c'était somptueux Mais y'a un ruisseau qui coule au fond d'tes yeux Et puis une lumière qui brûle au creux d'tes joues J'voulais juste l'avoir avant de devenir fou J'suis juste venu te dire faut être heureux On lève nos verres à tous ces gens parfaits Cupides, asservis au système qui les paie On crie notre haine aux yeux de la justice On marche solidaire mais finalement on sait qu'on n'y croit plus Et on boit des paroles non digérées Et puis on vide nos images téléchargées On crée des situations mieux négociées Parce qu'on nous a dit: un jour, ça va péter Je suis venu te dire faut être heureux Rien a foutre tu sais je veux le lire au fond d'tes yeux Je suis venu te dire faut être heureux Et puis après on verra ...qu'est ce que tu veux C'est des conneries, la vie elle est belle et c'est tant mieux Je suis venu te dire faut être heureux Faut être heureux!!";
let dir;
let pos;
let dirs = [-1, 0, 1];
let lastLetter = "";
let dirsForLetter = [
  {
    letters: "ajs",
    dirs: [0, 1],
  },
  {
    letters: "bkt",
    dirs: [0, -1],
  },
  {
    letters: "clu",
    dirs: [1, 0],
  },
  {
    letters: "dmv",
    dirs: [-1, 0],
  },
  {
    letters: "enw",
    dirs: [-1, -1],
  },
  {
    letters: "fox",
    dirs: [-1, 1],
  },
  {
    letters: "gpy",
    dirs: [1, 1],
  },
  {
    letters: "hqz",
    dirs: [1, -1],
  },
  {
    letters: "ir",
    dirs: [-1, -1],
  },
];
let alphabet = "abcdefghijklmnopqrtuvwxyz";

function setup() {
  canvas = createCanvas(800, 800);
  background(0);
  const dL = dirsForLetter.find((diL) =>
    diL.letters.includes(text[0].toLowerCase())
  );
  const d = dL ? dL.dirs : [1, 0];
  dir = createVector(d[0], d[1]);
  pos = createVector(0, 0);
}

function draw() {
  translate(width / 2, height / 2);
  for (let j = 0; j < 100; j++) {
    const c = text.charAt(i);

    if (!c) {
      noLoop();
      console.log("done");
      return;
    }

    if (c === " ") {
      noStroke();
      // fill(255, map(i, 0, text.length, 100, 10));
      fill(255);
      circle(pos.x, pos.y, 8);
      const dL = dirsForLetter.find((diL) =>
        diL.letters.includes(lastLetter.toLowerCase())
      );
      const d = dL ? dL.dirs : [1, 0];
      dir = createVector(d[0], d[1]);
    } else {
      pos.add(p5.Vector.mult(dir, alphabet.indexOf(c.toLowerCase()) + 1));
      lastLetter = c;
      edges();
    }
    fill(255);
    circle(pos.x, pos.y, 2);

    i++;
  }
}

function edges() {
  if (pos.x > width / 2 + 10) pos.x = -width / 2;
  else if (pos.x < -width / 2 - 10) pos.x = width / 2;
  else if (pos.y > height / 2 + 10) pos.y = -height / 2;
  else if (pos.y < -height / 2 - 10) pos.y = height / 2;
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
