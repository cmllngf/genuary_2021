// One process grows, another process prunes
let canvas;
const n = 60;
let tt = 0;

function setup() {
  canvas = createCanvas(1080, 1080);
  capturer.start();
}

function draw() {
  translate(width / 2.35, height / 2);
  background(0);
  colorMode(HSB);

  let numF = 100;
  const K = 20;
  const t = frameCount / numF;

  for (let i = 0; i < K; i++) {
    const p = (1.0 * (i + t)) / K;
    drawDot(p);
  }
  capturer.capture(canvas.canvas);
  if (frameCount == numF) {
    // console.log("done");
    // frameCount = 0;
    console.log(frameCount);
    capturer.stop();
    capturer.save();
    noLoop();
  }
}

function rotation(p) {
  return map(p, 0, 1, 0, 7 * TWO_PI);
}

function position(p) {
  const q = sqrt(p);
  const theta = 2.3 * TWO_PI * q;
  const r = constrain(0.4 * width * p * 1.5, 0, 0.4 * width);

  return createVector(r * cos(theta), r * sin(theta));
}

function dotSize(p) {
  if (p > 1) {
    return 0;
  }
  if (p < 0.8) return map(p, 0, 0.8, 0, 50, true);
  return map(p, 0.8, 1, 50, 0, true);
}

// function drawDot(p) {
//   const n = myNoise(p);
//   const pos = position(p);
//   noStroke();
//   fill(myNoise(p, 30) * 500, 58, 70);
//   // if (p < 0.8)
//   // circle(pos.x, pos.y, dotSize(p));
//   const dd = 20;
//   circle(
//     pos.x + map(n, 0, 1, -dd, dd),
//     pos.y + map(n, 0, 1, -dd, dd),
//     dotSize(p)
//   );
//   // else rect(pos.x, pos.y, 5, 5);
// }

function drawDot(p) {
  randomSeed(1);
  // const n = myNoise(p);
  const pos = position(p);
  const dots = dotSize(p);
  noStroke();
  // fill(myNoise(p, 30) * 500, 58, 70);

  for (let i = 0; i < 300; i++) {
    let a = random(TWO_PI);
    let y = random(-1, 1);
    let r = sqrt(1 - y * y);
    let c = myNoise(p, 10) * 450;
    a += rotation(p);
    let z = sin(a);
    fill(c, 78, 100);
    if (z > 0)
      circle(cos(a) * dots * r + pos.x, y * dots + z * r * 5 + pos.y, 3);
  }
}

function myNoise(p, scl = 30) {
  return pow((noise(scl * p) + 1) / 2.0, 2.0);
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
