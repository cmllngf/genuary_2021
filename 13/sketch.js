// Do not repeat
let canvas;
let lengths = [];
let seed;
let t = 0;
let word = [
  [0, 0, 0, 0],
  [0, 1],
  [0, 1, 0, 0],
  [0, 1, 0, 0],
  [0, 0, 1],
  [1, 0, 1, 0],
  [0, 0],
  [1, 0],
  [0, 1],
  [1],
  [0, 0],
  [1, 1, 1],
  [1, 0],
  [0, 0, 0],
];
let word2 = [
  [1, 0, 1, 0],
  [1, 1, 1],
  [0, 1, 0, 0],
  [0, 1, 0, 0],
  [0],
  [1, 0, 1, 0],
  [1],
  [0, 0],
  [0, 0, 0, 1],
  [0],
  [0, 0, 0],
];

function setup() {
  canvas = createCanvas(800, 800);
  background(0);
  for (let i = 0; i < 14; i++) {
    lengths.push(int(random(3, 13)));
  }
  seed = random(99999);
}

function draw() {
  // translate(width / 2, height / 2);
  // strokeCap(SQUARE);
  randomSeed(seed);
  background(0);
  // background(0, 70);
  // for (let i = 0; i < lengths.length; i++) {
  //   let y = 45;
  //   // for (let j = 0; j < map(noise(i), 0, 1, 3, 13); j++) {
  //   for (let j = 0; j < lengths[i]; j++) {
  //     const x = 55 * i + 42.5;
  //     if (noise(j, i, t) >= 0.5) {
  //       strokeCap(PROJECT);
  //       stroke(255);
  //       strokeWeight(10);
  //       noFill();
  //       rect(20, 20, 760, 760);
  //       line(x, y, x, y + 42.5);
  //       y += 70;
  //     } else {
  //       noStroke();
  //       fill(255);
  //       circle(x, y, 10);
  //       y += 30;
  //     }
  //   }
  // }
  for (let i = 0; i < word.length; i++) {
    let y = 45;
    // for (let j = 0; j < map(noise(i), 0, 1, 3, 13); j++) {
    for (let j = 0; j < word[i].length; j++) {
      const x = 55 * i + 42.5;
      if (word[i][j] == 1) {
        strokeCap(PROJECT);
        stroke(255);
        strokeWeight(10);
        noFill();
        rect(20, 20, 760, 760);
        line(x, y, x, y + 42.5);
        y += 70;
      } else {
        noStroke();
        fill(255);
        circle(x, y, 10);
        y += 30;
      }
    }
  }
  for (let i = 0; i < word2.length; i++) {
    let y = 755;
    for (let j = 0; j < word2[i].length; j++) {
      const x = 70 * i + 51;
      if (word2[i][j] == 1) {
        strokeCap(PROJECT);
        stroke(255);
        strokeWeight(10);
        noFill();
        rect(20, 20, 760, 760);
        line(x, y, x, y - 42.5);
        y -= 70;
      } else {
        noStroke();
        fill(255);
        circle(x, y, 10);
        y -= 30;
      }
    }
  }
  noLoop();
  t += 0.005;
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
