class System {
  constructor(x, y, size, h = random(360), s = random(100), b = random(100)) {
    this.x = x;
    this.y = y;
    this.size = size;
    this.walkers = [];
    this.randomHue = random(360);
    this.h = h;
    this.s = s;
    this.b = b;
    this.seed = random(999999);

    for (let i = 0; i < 20; i++) {
      const wx = int(random(x, x + size));
      // const wy = int(random(y, y + size));
      const wy = y;
      this.walkers.push(this.createWalker(wx, wy));
    }
  }

  //returns false if finished
  update(visited, colors) {
    randomSeed(this.seed);
    for (let i = 0; i < 100; i++) {
      this.walkers.forEach((walker) => {
        // walker.update(visited, colors, pixel_size, color(random(450), 100, 58));
        // walker.update(
        //   visited,
        //   colors,
        //   pixel_size,
        //   color(this.randomHue, 70, 58)
        // );
        walker.update(visited, colors, pixel_size);
      });
    }
    this.walkers = this.walkers.filter((walker) => walker.alive);
    return this.walkers.length > 0 ? true : false;
  }

  createWalker(x = int(random(width)), y = int(random(height))) {
    const p = createVector(x, y);
    const h = random(360);
    const s = random(100);
    const b = random(100);
    const c = color(this.h, this.s, this.b);
    seed(p, c);
    return new Walker(
      p,
      -1,
      this.x,
      this.x + this.size,
      this.y,
      this.y + this.size,
      color(random(360), 70, 58)
    );
  }
}
