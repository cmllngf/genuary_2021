// small areas of symmetry
let seed;
let t = 0;
const sizes = [100, 150, 150, 200, 250, 300, 350, 400, 450, 500];
// let capturer;
let canvas;

function setup() {
  canvas = createCanvas(1080, 1080);
  console.log({ canvas });
  background(0);
  seed = random(99999);
  capturer.start();
}

function draw() {
  background(0);
  randomSeed(seed);
  translate(width / 2, height / 2);
  noStroke();
  fill(255);

  for (let j = 0; j < sizes.length; j++) {
    const n = sizes[j];
    for (let i = 0; i < n - 50; i++) {
      const a = (TWO_PI / (n - 50)) * i;
      const x = cos(a);
      const y = sin(a);
      const r = n + map(myNoise(x, y, sin(t)), 0, 1, -80, 80);
      circle(x * r, y * r, 3);
    }
  }
  t += 0.01;
  capturer.capture(canvas.canvas);
  if (t >= TWO_PI) {
    console.log(frameCount);
    capturer.stop();
    capturer.save();
    noLoop();
  }
  // noLoop();
}

function myNoise(p, p2, p3 = 0) {
  const scl = 60;
  return pow((noise(scl * p, scl * p2, p3) + 1) / 2.0, 17.0);
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
