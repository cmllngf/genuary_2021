// Draw a line. Wrong answers only.
let canvas;
let seed;
let t = 0;
const animStart2 = 70;
const animStart = 10;
const backgroundDash = [];

function setup() {
  canvas = createCanvas(800, 800);
  seed = random(99999);
  for (let i = 0; i < 100; i++) {
    backgroundDash.push({
      x: int(random(800)),
      y: int(random(800)),
      z: int(random(6)),
    });
  }
  capturer.start();
}

let addT = 0.2;
let rever = false;
let maxT = 0;

function draw() {
  randomSeed(seed);
  background(0);
  strokeCap(PROJECT);
  for (let i = 0; i < backgroundDash.length; i++) {
    const { x, y, z } = backgroundDash[i];
    strokeWeight(map(z, 0, 6, 1, 3));
    stroke(map(z, 0, 6, 50, 150));
    line(x, y, x + map(z, 0, 6, 4, 10), y);
    backgroundDash[i] = {
      ...backgroundDash[i],
      x: x < 0 ? 800 : x - map(z, 0, 6, 0.7, 4),
    };
  }
  noStroke();
  if (t >= animStart) {
    if (t >= animStart2) {
      let tt = 1;
      for (let x = 450; x > -10; x--) {
        let ttt = 0;
        if (t >= animStart2) ttt = constrain(tt, 0, 90);
        for (let i = 0; i < 3; i++) {
          fill(255);
          circle(
            x,
            400 +
              sin(
                (x + (i + 1) * [355, 40, 464][i]) *
                  map(t, animStart2, 10000, 0.00001, 1, true)
              ) *
                ttt,
            5
          );
        }
        tt += 0.2;
      }
    } else {
      const nP = map(t, animStart, animStart2 - animStart2 * 0.1, 0, 450, true);
      for (let x = 0; x < nP; x++) {
        circle(x, 400, 5);
      }
    }
  }
  t += addT;
  capturer.capture(canvas.canvas);
  if (t >= 150) {
    console.log(frameCount);
    capturer.stop();
    capturer.save();
    noLoop();
  }
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
