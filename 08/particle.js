class Particle {
  constructor(x, y) {
    this.pos = createVector(x, y);
    this.vel = createVector(0, 0);
    this.acc = createVector(0, 0);
    this.maxSpeed = 2;
  }

  update() {
    this.vel.add(this.acc);
    this.vel.limit(this.maxSpeed);
    this.pos.add(this.vel);
    this.acc.mult(0);
  }

  applyForce(force) {
    this.acc.add(force);
  }

  follow() {
    const x = floor(this.pos.x / scl);
    const y = floor(this.pos.y / scl);
    const vector = noiseField[y * (width / scl) + x];
    this.applyForce(vector);
  }

  display() {
    noStroke();
    fill(255);

    const yy =
      map(
        dist(this.pos.x, this.pos.y, width / 2, height / 2),
        0,
        250,
        80,
        0,
        true
      ) * (this.pos.y < height / 2 ? -1 : 1);

    circle(this.pos.x, this.pos.y + yy, 3);
  }
}
