// Curve Only
var noiseMultiplier = 0.1;
var noiseField = [];
var scl = 20;

let ps = [];

function setup() {
  createCanvas(800, 800);
  background(0);
  for (let y = -200; y < (height / scl) * 2; y++) {
    for (let x = -200; x < (width / scl) * 2; x++) {
      const phi = map(
        noise(x * noiseMultiplier, y * noiseMultiplier),
        0,
        1,
        -PI / 6,
        PI / 6
      );
      const v = p5.Vector.fromAngle(phi);
      v.setMag(0.1);
      noiseField[y * (width / scl) + x] = v;
    }
  }

  for (let i = -100; i < height * 2; i += 30) {
    ps.push(new Particle(0, i));
  }
}

function draw() {
  noStroke();
  fill(255);

  for (let i = 0; i < ps.length; i++) {
    ps[i].follow();
    ps[i].update();
    ps[i].display();
  }

  // for (let y = 0; y < height; y += 30) {
  //   for (let x = 0; x < width; x++) {
  //     const yy =
  //       map(dist(x, y, width / 2, height / 2), 0, 250, 100, 0, true) *
  //       (y < height / 2 ? -1 : 1);
  //     circle(x, y + yy, 2);
  //   }
  // }
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
