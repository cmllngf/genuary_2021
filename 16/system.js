class System {
  constructor(x, y, width, height, col1, col2) {
    this.circles = [];
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.col1 = col1;
    this.col2 = col2;
    for (let i = 0; i < opts.Particles; i++) {
      this.circles.push(
        new Particle(
          createVector(
            random(x + width / 2 - 10, x + width / 2 + 10),
            random(y + height / 2 - 10, y + height / 2 + 10)
          ),
          random() > opts.White_Amount
        )
      );
    }
  }

  update(t) {
    for (let i = 0; i < opts.Particles; i++) {
      this.circles[i].display(t, this.col1, this.col2);
      this.circles[i].update(this.x, this.y, this.width, this.height);
      for (let j = i + 1; j < opts.Particles; j++) {
        if (this.circles[i].overlaps(this.circles[j])) {
          this.circles[i].avoid(this.circles[j]);
          this.circles[j].avoid(this.circles[i]);
        }
      }
    }
  }
}
