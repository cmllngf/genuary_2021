// Circles Only
const opts = {
  width: 800,
  height: 800,

  Particles: 800,
};

let system1;
let system2;
let t = 0;

function setup() {
  createCanvas(opts.width, opts.height);
  background(0);
  system1 = new System(0, 0, 800, 800, "darkColor", "lightColor");
  // system2 = new System(0, 0, 800, 800, "lightColor", "darkColor");
}

function draw() {
  for (let i = 0; i < 1; i++) {
    system1.update(t);
    // system2.update(t);
    t += 0.01;
  }
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
