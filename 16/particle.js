class Particle {
  constructor(pos = createVector(random(width), random(height)), colored) {
    this.firstPos = pos.copy();
    this.pos = pos;
    this.vel = createVector(0, 0);
    this.acc = createVector(0, 0);
    this.radius = 25;
    this.colored = colored;
    this.maxSpeed = random(5, 6);
    this.radiusVariation = random(-20, 20);
    this.lightColor = random(190, 230);
    this.darkColor = random(10, 30);
    this.filled = random() > 0.9;
  }

  update(x, y, width, height) {
    this.vel.add(this.acc);
    this.vel.limit(this.maxSpeed);
    this.pos.add(this.vel);
    this.acc.mult(0);
    this.vel.mult(0.9);
    this.edges(x, y, width, height);
  }

  applyForce(force) {
    this.acc.add(force);
  }

  display(t, col1, col2) {
    const d = dist(this.pos.x, this.pos.y, width / 2, height / 2);
    // stroke(cos((this.pos.x && this.pos.y) * .02) < .5 ? this[col1] : this[col2])
    if (this.filled) {
      noStroke();
      fill(d > 250 ? this[col1] : this[col2]);
    } else {
      noFill();
      stroke(d > 250 ? this[col1] : this[col2]);
    }
    //TODO noise c'est pas mal ici
    // const v = noise(this.pos.x * 0.01, this.pos.y * 0.01, d * 0.001 + t);
    // stroke(v > 0.5 ? this.darkColor : this.lightColor);
    circle(this.pos.x, this.pos.y, this.radius * 2);
  }

  edges(x, y, width, height) {
    if (this.pos.x < x + this.radius || this.pos.x + this.radius > x + width) {
      this.vel.x = -this.vel.x;
    }
    if (this.pos.y < y + this.radius || this.pos.y + this.radius > y + height) {
      this.vel.y = -this.vel.y;
      if (this.vel.y < y + 0.05 && this.vel.y > y + height - 0.05)
        this.vel.y = random(-1, 1);
      if (this.vel.x < x + 0.05 && this.vel.x > x + width - 0.05)
        this.vel.x = random(-1, 1);
    }
  }

  overlaps(other) {
    const d = dist(this.pos.x, this.pos.y, other.pos.x, other.pos.y);
    return d < this.radius + other.radius;
  }

  avoid(other) {
    const a = atan2(other.pos.y - this.pos.y, other.pos.x - this.pos.x);
    const force = p5.Vector.fromAngle(a).mult(-0.4);
    this.applyForce(force);
  }

  setColor(c) {
    this.c = c;
  }
}
